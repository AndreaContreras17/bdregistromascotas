/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.registrodemascota.dao;

import com.mycompany.registrodemascota.dao.exceptions.NonexistentEntityException;
import com.mycompany.registrodemascota.dao.exceptions.PreexistingEntityException;
import com.mycompany.registrodemascota.entity.Registrodemascotas;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author andre
 */
public class RegistrodemascotasJpaController implements Serializable {

    public RegistrodemascotasJpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("mascotas_PU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Registrodemascotas registrodemascotas) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(registrodemascotas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRegistrodemascotas(registrodemascotas.getRut()) != null) {
                throw new PreexistingEntityException("Registrodemascotas " + registrodemascotas + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Registrodemascotas registrodemascotas) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            registrodemascotas = em.merge(registrodemascotas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = registrodemascotas.getRut();
                if (findRegistrodemascotas(id) == null) {
                    throw new NonexistentEntityException("The registrodemascotas with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Registrodemascotas registrodemascotas;
            try {
                registrodemascotas = em.getReference(Registrodemascotas.class, id);
                registrodemascotas.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The registrodemascotas with id " + id + " no longer exists.", enfe);
            }
            em.remove(registrodemascotas);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Registrodemascotas> findRegistrodemascotasEntities() {
        return findRegistrodemascotasEntities(true, -1, -1);
    }

    public List<Registrodemascotas> findRegistrodemascotasEntities(int maxResults, int firstResult) {
        return findRegistrodemascotasEntities(false, maxResults, firstResult);
    }

    private List<Registrodemascotas> findRegistrodemascotasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Registrodemascotas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Registrodemascotas findRegistrodemascotas(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Registrodemascotas.class, id);
        } finally {
            em.close();
        }
    }

    public int getRegistrodemascotasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Registrodemascotas> rt = cq.from(Registrodemascotas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
