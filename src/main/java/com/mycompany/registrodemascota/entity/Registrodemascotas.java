/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.registrodemascota.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author andre
 */
@Entity
@Table(name = "registrodemascotas")
@NamedQueries({
    @NamedQuery(name = "Registrodemascotas.findAll", query = "SELECT r FROM Registrodemascotas r"),
    @NamedQuery(name = "Registrodemascotas.findByNombre", query = "SELECT r FROM Registrodemascotas r WHERE r.nombre = :nombre"),
    @NamedQuery(name = "Registrodemascotas.findByRut", query = "SELECT r FROM Registrodemascotas r WHERE r.rut = :rut"),
    @NamedQuery(name = "Registrodemascotas.findByNombremascota", query = "SELECT r FROM Registrodemascotas r WHERE r.nombremascota = :nombremascota"),
    @NamedQuery(name = "Registrodemascotas.findByRaza", query = "SELECT r FROM Registrodemascotas r WHERE r.raza = :raza")})
public class Registrodemascotas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rut")
    private String rut;
    @Size(max = 2147483647)
    @Column(name = "nombremascota")
    private String nombremascota;
    @Size(max = 2147483647)
    @Column(name = "raza")
    private String raza;

    public Registrodemascotas() {
    }

    public Registrodemascotas(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombremascota() {
        return nombremascota;
    }

    public void setNombremascota(String nombremascota) {
        this.nombremascota = nombremascota;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Registrodemascotas)) {
            return false;
        }
        Registrodemascotas other = (Registrodemascotas) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.registrodemascota.entity.Registrodemascotas[ rut=" + rut + " ]";
    }
    
}
