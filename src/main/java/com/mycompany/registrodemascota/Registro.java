/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.registrodemascota;

import com.mycompany.registrodemascota.dao.RegistrodemascotasJpaController;
import com.mycompany.registrodemascota.dao.exceptions.NonexistentEntityException;
import com.mycompany.registrodemascota.entity.Registrodemascotas;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author andre
 */
@Path("registro")
public class Registro {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listadoIngresos(){
        
       /* Registrodemascotas tdm= new Registrodemascotas();
        tdm.setNombre("manuel");
        tdm.setRut("6.529.018-5");
        tdm.setNombremascota("copito");
        tdm.setRaza("beagle");
        
        Registrodemascotas tdm2= new Registrodemascotas();
        tdm2.setNombre("teresa");
        tdm2.setRut("12.845.891-0");
        tdm2.setNombremascota("luna");
        tdm2.setRaza("siames");
        
        List<Registrodemascotas> lista = new ArrayList<Registrodemascotas>();
        
        lista.add(tdm);
        lista.add(tdm2);*/
        
       RegistrodemascotasJpaController dao=new RegistrodemascotasJpaController();
       
      List<Registrodemascotas> lista= dao.findRegistrodemascotasEntities();
       
        return Response.ok(25).entity(lista).build();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(Registrodemascotas Registro){
      
        try {
            RegistrodemascotasJpaController dao=new RegistrodemascotasJpaController();
            dao.create(Registro);
        } catch (Exception ex) {
            Logger.getLogger(Registro.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(25).entity(Registro).build();
    }
    
    @DELETE
    @Path("/(iddelete)")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("iddelete") String iddelete){
      
       
        try {
            RegistrodemascotasJpaController dao=new RegistrodemascotasJpaController();
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(Registro.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok("registro eliminado").build();
    }
    
    @PUT
    public Response update(Registrodemascotas Registro){
        
        try {
            RegistrodemascotasJpaController dao=new RegistrodemascotasJpaController();
            dao.edit(Registro);
        } catch (Exception ex) {
            Logger.getLogger(Registro.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        return Response.ok(25).entity(Registro).build();
    }
}
